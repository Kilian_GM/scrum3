package SCRUM3;

public class Gratuit extends Evento {

    public Gratuit() {
    }

    public Gratuit(String nombre, String categoria, String organizador, String dirección, int horaInicio, int horaFin, int fecha) {
        super(nombre, categoria, organizador, dirección, horaInicio, horaFin, fecha);
    }
}