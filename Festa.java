package SCRUM3;

import java.util.ArrayList;

public class Festa {
    private ArrayList<Evento> listaEventos;
    private ArrayList<User> listaUsers;

    public Festa() {
    }

    public Festa(ArrayList<Evento> listaEventos, ArrayList<User> listaUsers) {
        this.listaEventos = listaEventos;
        this.listaUsers = listaUsers;
    }

    public ArrayList<User> getListaUsers() {
        return listaUsers;
    }

    public void crear_evento(Evento e1){
        listaEventos.add(e1);
    }

    public void eliminar_evento(int e1){
        listaEventos.remove(e1);
    }

    public void mostrarInfo(){
        for(int i = 0; i < listaEventos.size(); i++){
            System.out.println(i+1 + ". " + listaEventos.get(i).toString());
        }
    }

    public void crear_usuario(User u1){
        listaUsers.add(u1);
    }
}