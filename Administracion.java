package SCRUM3;

public interface Administracion {
    void añadirEvento();
    void eliminarEvento();
    void modificarEvento();
}
