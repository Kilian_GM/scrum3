package SCRUM3;

import java.util.Scanner;

public class Admin extends User implements Administracion {
    private Scanner sc = new Scanner(System.in);

    public Admin(String nom, String apellido, String dni, int edad, int movil, String mail, String nickname, String password) {
        super(nom, apellido, dni, edad, movil, mail, nickname, password);
    }


    public Admin(){}

    @Override
    public void registrarUser() {
        User user = new Admin();
        Festa f1= new Festa();
        f1.crear_usuario(user);
    }

    @Override
    public void añadirEvento() {
        System.out.println("Que tipo de evento desea añadir?");
        System.out.println("1. Evento Gratuito.");
        System.out.println("2. Evento de Pago.");
        int opc = sc.nextInt();
        switch (opc){
            case 1:
                Festa f1=new Festa();
                System.out.println("Introduce el nombre del evento:");
                String nombre = sc.nextLine();
                System.out.println("Introduce la categoria:");
                String categoria = sc.nextLine();
                System.out.println("Introduce el organizador:");
                String organizador = sc.nextLine();
                System.out.println("Introduce la dirección del evento:");
                String direccion = sc.nextLine();
                System.out.println("Introduce la hora de inicio y la hora de fin:");
                int hinicio = sc.nextInt();
                int hfin = sc.nextInt();
                System.out.println("Introduce la fecha de realización del evento:");
                int fecha = sc.nextInt();
                Evento envento = new Gratuit(nombre, categoria, organizador, direccion, hinicio, hfin, fecha );
                f1.crear_evento(envento);
                break;
            case 2:
                Festa f2=new Festa();
                System.out.println("Introduce el nombre del evento:");
                String n = sc.nextLine();
                System.out.println("Introduce la categoria:");
                String c = sc.nextLine();
                System.out.println("Introduce el organizador:");
                String o = sc.nextLine();
                System.out.println("Introduce la dirección del evento:");
                String d = sc.nextLine();
                System.out.println("Introduce la hora de inicio y la hora de fin:");
                int hi = sc.nextInt();
                int hf= sc.nextInt();
                System.out.println("Introduce la fecha de realización del evento:");
                int f = sc.nextInt();
                System.out.println("Introduce el precio de participación del evento:");
                int p = sc.nextInt();
                Evento envent = new Pago(n, c, o, d, hi, hf, f, p);
                f2.crear_evento(envent);
                break;
        }
    }

    @Override
    public void eliminarEvento() {
        Festa f1= new Festa();
        f1.mostrarInfo();
        System.out.println("Selecciona el evento que deseas eliminar:");
        f1.eliminar_evento(sc.nextInt());
    }

    @Override
    public void modificarEvento() {

    }

    @Override
    public void buscarEvento() {

    }

    @Override
    public void verEventos() {

    }
}
