package SCRUM3;

public class Pago extends Evento {
    private int precio;

    public Pago(int precio) {
        this.precio = precio;
    }

    public Pago(String nombre, String categoria, String organizador, String dirección, int horaInicio, int horaFin, int fecha, int precio) {
        super(nombre, categoria, organizador, dirección, horaInicio, horaFin, fecha);
        this.precio = precio;
    }

    public Pago(){}
}
