package SCRUM3;

public abstract class User implements  Busqueda{
    private String nom;
    private String apellido;
    private String dni;
    private int edad;
    private int movil;
    private String mail;
    private String nickname;
    private String password;

    public User(String nom, String apellido, String dni, int edad, int movil, String mail, String nickname, String password) {
        this.nom = nom;
        this.apellido = apellido;
        this.dni = dni;
        this.edad = edad;
        this.movil = movil;
        this.mail = mail;
        this.nickname = nickname;
        this.password = password;
    }

    public User() {
    }

    public String getNom() {
        return nom;
    }

    public String getApellido() {
        return apellido;
    }

    public String getDni() {
        return dni;
    }

    public int getEdad() {
        return edad;
    }

    public int getMovil() {
        return movil;
    }

    public String getMail() {
        return mail;
    }

    public String getNickname() {
        return nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setMovil(int movil) {
        this.movil = movil;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    abstract public void registrarUser();
}


