package SCRUM3;

public abstract class Evento {
    private String nombre;
    private String categoria;
    private String organizador;
    private String dirección;
    private int horaInicio;
    private int horaFin;
    private int fecha;

    public Evento() {
    }

    public Evento(String nombre, String categoria, String organizador, String dirección, int horaInicio, int horaFin, int fecha) {
        this.nombre = nombre;
        this.categoria = categoria;
        this.organizador = organizador;
        this.dirección = dirección;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getOrganizador() {
        return organizador;
    }

    public String getDirección() {
        return dirección;
    }

    public int getHoraInicio() {
        return horaInicio;
    }

    public int getHoraFin() {
        return horaFin;
    }

    public int getFecha() {
        return fecha;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setOrganizador(String organizador) {
        this.organizador = organizador;
    }

    public void setDirección(String dirección) {
        this.dirección = dirección;
    }

    public void setHoraInicio(int horaInicio) {
        this.horaInicio = horaInicio;
    }

    public void setHoraFin(int horaFin) {
        this.horaFin = horaFin;
    }

    public void setFecha(int fecha) {
        this.fecha = fecha;
    }
}
